#!/usr/bin/env python3
import os
import time

import boto3
import botocore

CFN = boto3.client('cloudformation', region_name=os.environ['AWS_REGION'])
SSM = boto3.client('ssm', region_name=os.environ['AWS_REGION'])


class SsmExecutionError(Exception):
    pass


def check_ebs_snapshot_state(snapshot_id, region):
    print(f'Checking EBS snapshot state in {region} for {snapshot_id}')
    boto_config = botocore.config.Config(retries={'max_attempts': 10})
    ec2 = boto3.client('ec2', region_name=region, config=boto_config)
    try:
        ebs_snapshot_status = ec2.describe_snapshots(SnapshotIds=[snapshot_id])
    except botocore.exceptions.ClientError as boto_err:
        if boto_err.response['Error']['Code'] == 'Throttling':
            print(f'describe_snapshots was throttled; returning to waiter')
            return 'waiting'
        raise
    ebs_snapshot_state = ebs_snapshot_status['Snapshots'][0]['State']
    print(f"Snapshot status for {snapshot_id}: {ebs_snapshot_state}")
    return ebs_snapshot_state


def check_rds_snapshot_state(snapshot_id, region):
    print(f'Checking RDS snapshot state in {region} for {snapshot_id}')
    boto_config = botocore.config.Config(retries={'max_attempts': 10})
    rds = boto3.client('rds', region_name=region, config=boto_config)
    try:
        db_snapshot_status = rds.describe_db_snapshots(DBSnapshotIdentifier=snapshot_id)
    except botocore.exceptions.ClientError as boto_err:
        if boto_err.response['Error']['Code'] == 'Throttling':
            print(f'describe_db_snapshots was throttled; returning to waiter')
            return 'waiting'
        raise
    rds_snapshot_state = db_snapshot_status['DBSnapshots'][0]['Status']
    print(f'Snapshot status for {snapshot_id}: {rds_snapshot_state}')
    return rds_snapshot_state


def get_stack_resource_id(stack_name, logical_id):
    stack_resource = CFN.describe_stack_resource(
        LogicalResourceId=logical_id, StackName=stack_name
    )
    return stack_resource['StackResourceDetail']['PhysicalResourceId']


def ssm_wait_response(backmac_instance, cmd):
    ssm_command = SSM.send_command(
        InstanceIds=[backmac_instance.instance_id],
        DocumentName='AWS-RunShellScript',
        Parameters={'commands': [cmd]},
        OutputS3BucketName='wpe-logs',
        OutputS3KeyPrefix='run-command-logs',
    )

    print(f'for command: {cmd} CommandId is: {ssm_command["Command"]["CommandId"]}')
    status = 'Pending'

    while status in ('Pending', 'InProgress'):
        time.sleep(3)
        list_command = SSM.list_commands(CommandId=ssm_command['Command']['CommandId'])
        status = list_command['Commands'][0]['Status']

    result = SSM.get_command_invocation(
        CommandId=ssm_command['Command']['CommandId'],
        InstanceId=backmac_instance.instance_id,
    )

    if result['ResponseCode'] != 0:
        raise SsmExecutionError(
            f'SSM command failed! Error: {result["StandardErrorContent"]}'
        )

    return result['StandardOutputContent']


def store_lambda_env(event, context):
    for param in ['function_name', 'log_group_name', 'log_stream_name']:
        try:
            event[f'current_{param}'] = getattr(context, param)
        except AttributeError:
            pass
