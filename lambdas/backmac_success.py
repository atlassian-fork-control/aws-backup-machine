#!/usr/bin/env python3

import os
import pprint
import time

import backmac_notify as notify


def lambda_handler(event, context):
    # we're always coming from a parallel execution block; merge the events
    event = {**event[0], **event[1]}
    event['end_tstamp'] = time.time()
    print(
        notify.send('success', f'Iteration successful for {event["stack_name"]}', event)
    )
    return event


if __name__ == "__main__":
    import random

    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"
    start_tstamp = time.time() - random.randint(30, 10000)
    event = [
        {
            "backmac_instance": "i-00000000000000000",
            "backup_machine": "backup_machine",
            "dr_region": "us-east-1",
            "ebs2dr_status": "completed",
            "ebs_backup_vol": "vol-00000000000000000",
            "ebs_dr_snap": "snap-00000000000000000",
            "ebs_snap": "snap-00000000000000000",
            "rds2dr_status": "waiting",
            "rds_snap": "jira-stack-snap-000000000000",
            "rds_snap_arn": "arn:aws:rds:us-west-2:000000000000:snapshot:jira-stack-snap-000000000000",
            "rsync_command_id": "836f6970-0fe1-4e10-a233-000000000000",
            "rsync_status": "Success",
            "run_id": "20190326-205156-900129",
            "security_group_id": "sg-abcdefgh",
            "stack_name": "jira-stack",
            "start_tstamp": start_tstamp,
        },
        {
            "backmac_instance": "i-00000000000000000",
            "backup_machine": "backup_machine",
            "dr_region": "us-east-1",
            "ebs_backup_vol": "vol-00000000000000000",
            "ebs_snap": "snap-00000000000000000",
            "rds2dr_status": "waiting",
            "rds_snap": "jira-stack-snap-000000000000",
            "rds_snap_arn": "arn:aws:rds:us-west-2:000000000000:snapshot:jira-stack-snap-000000000000",
            "rsync_command_id": "836f6970-0fe1-4e10-a233-000000000000",
            "rsync_status": "Success",
            "run_id": "20190326-205156-900129",
            "security_group_id": "sg-abcdefgh",
            "stack_name": "jira-stack",
            "start_tstamp": start_tstamp,
        },
    ]
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
