#!/usr/bin/env python3

import os
import pprint
import time

import backmac_utils
import boto3
import botocore
from botocore.config import Config


def get_backup_volume_id(stack_name):
    config = Config(retries=dict(max_attempts=10))
    ec2 = boto3.client('ec2', region_name=os.environ['AWS_REGION'], config=config)
    volumes = ec2.describe_volumes(
        Filters=[{'Name': 'tag:Name', 'Values': [stack_name + "-backup"]}]
    )
    print(("backup volumeId is: ", volumes['Volumes'][0]['VolumeId']))
    return volumes['Volumes'][0]['VolumeId']


def attach_volume(backmac_instance, backup_volume_id):
    # add the service security group to backmac node
    ec2 = boto3.resource('ec2', region_name=os.environ['AWS_REGION'])
    backup_volume = ec2.Volume(backup_volume_id)
    try:
        backup_volume.attach_to_instance(
            Device='/dev/xvdz', InstanceId=backmac_instance
        )
    except botocore.exceptions.ClientError as e:
        print(e)
        if e.response['Error']['Code'] == 'VolumeInUse':
            if backup_volume.attachments[0]['InstanceId'] == backmac_instance:
                print(
                    'this error occurs when the relevant volume is already mounted, thus can be ignored'
                )
            else:
                raise Exception(
                    f"backup volume {backup_volume_id} is attached to {backup_volume.attachments[0]['InstanceId']}, which is not the backmac instance for this run (should be {backmac_instance})!"
                )
    # wait for the volume to be attached
    waiter = ec2.meta.client.get_waiter('volume_in_use')
    waiter.wait(VolumeIds=[backup_volume_id])
    # ensure the device is available to the OS
    while True:
        file_response = ssm_wait_response(
            backmac_instance, f"file -bsL {backup_volume.attachments[0]['Device']}"
        )
        if "cannot open" in file_response[1]:
            time.sleep(2)
        else:
            break
    return backup_volume.attachments[0]['Device']


def ssm_wait_response(backmac_instance, cmd):
    ssm = boto3.client('ssm', region_name=os.environ['AWS_REGION'])
    ssm_command = ssm.send_command(
        InstanceIds=[backmac_instance],
        DocumentName='AWS-RunShellScript',
        Parameters={'commands': [cmd]},
        OutputS3BucketName='wpe-logs',
        OutputS3KeyPrefix='run-command-logs',
    )
    print(
        ("for command: ", cmd, " command_Id is: ", ssm_command['Command']['CommandId'])
    )
    status = 'Pending'
    while status == 'Pending' or status == 'InProgress':
        time.sleep(3)
        list_command = ssm.list_commands(CommandId=ssm_command['Command']['CommandId'])
        status = list_command['Commands'][0]['Status']
    result = ssm.get_command_invocation(
        CommandId=ssm_command['Command']['CommandId'], InstanceId=backmac_instance
    )
    return result['ResponseCode'], result['StandardOutputContent']


def ensure_volume_filesystem(backmac_instance, backup_volume_id, device):
    blkid_response = ssm_wait_response(backmac_instance, f"blkid {device}")
    print(blkid_response)
    if blkid_response[0] == 2:
        print(("volume " + backup_volume_id + " has no filesystem - creating ext4"))
        mkfs_response = ssm_wait_response(backmac_instance, f"mkfs -t ext4 {device}")
        print(mkfs_response)
    else:
        resize_fs(backmac_instance, device)


def cleanmount_ebs(backmac_instance, backup_volume_id):
    cmd = 'umount -f /backup'
    cmd_response = ssm_wait_response(backmac_instance, cmd)
    print(cmd_response)
    # really really sorry about this for whoever needs to maintain it
    cmd = (
        "mountdev=$(awsvol=$(echo "
        + backup_volume_id
        + "|cut -d- -f2);nvme list|grep ${awsvol}|awk '{print $1}'); mount -t ext4 ${mountdev} /backup"
    )
    cmd_response = ssm_wait_response(backmac_instance, cmd)
    return cmd_response[0]


def resize_fs(backmac_instance, device):
    """This attempts to resize the filesystem to it's maximum size. It's needed after a volume resize,
    and won't do any damage if the filesystem is already at its maximum size.
    """
    cmd_response = ssm_wait_response(backmac_instance, f"resize2fs {device}")
    if cmd_response[0] == 1:
        # resize2fs failed, try e2fsck first:
        ssm_wait_response(backmac_instance, f'umount /backup')
        e2fsck_response = ssm_wait_response(backmac_instance, f'e2fsck -f {device}')
        ssm_wait_response(backmac_instance, f'mount -t ext4 {device} /backup')
        cmd_response = ssm_wait_response(backmac_instance, f"resize2fs {device}")
    return cmd_response


def detach_volume(backmac_instance, volume_id):
    # add the service security group to backmac node
    ec2 = boto3.resource('ec2', region_name=os.environ['AWS_REGION'])
    backup_volume = ec2.Volume(volume_id)
    detached_volume_resp = backup_volume.detach_from_instance(
        Device='/dev/xvdz', InstanceId=backmac_instance
    )
    print(detached_volume_resp)


def get_volume_size(backmac_instance):
    total_space = ssm_wait_response(
        backmac_instance, "df /backup | tail -n 1 | awk '{print $2}'"
    )
    return total_space[1].rstrip()


def get_ebs_used_percent(backmac_instance):
    total_used_percent = ssm_wait_response(
        backmac_instance, "df /backup | tail -n 1 | awk '{print $5}' | sed 's/%//'"
    )
    return total_used_percent[1].rstrip()


def lambda_handler(event, context):
    backmac_utils.store_lambda_env(event, context)
    stack_name = event['stack_name']
    try:
        backmac_instance = event['backmac_instance']
    except:
        raise Exception('No backmac_instance found in event')
    backup_volume_id = get_backup_volume_id(stack_name)
    # collect some current state stuff for diagnostics if fail
    cmd = 'df -h;lsblk;nvme list'
    cmd_response = ssm_wait_response(backmac_instance, cmd)
    print(cmd_response)

    event['backup_vol_device_name'] = attach_volume(backmac_instance, backup_volume_id)
    cmd_response = ssm_wait_response(backmac_instance, "mkdir -p /backup")
    ensure_volume_filesystem(
        backmac_instance, backup_volume_id, event['backup_vol_device_name']
    )
    cmd_response = cleanmount_ebs(backmac_instance, backup_volume_id)
    if cmd_response != 0:
        print(
            (
                "volume "
                + backup_volume_id
                + " did not mount cleanly to /backup - check SSM for error - exiting"
            )
        )
        exit(1)
    event['ebs_backup_vol'] = backup_volume_id
    event['ebs_size'] = get_volume_size(backmac_instance)
    event['ebs_used_percent'] = get_ebs_used_percent(backmac_instance)
    # Resize if /backup ebs space is 100 percent or if difference between ebs and efs size is less than 15 percent
    if (
        (int(event['ebs_size']) - int(event['efs_used_space']))
        / int(event['ebs_size'])
        * 100
    ) <= 15 or int(event['ebs_used_percent']) >= 90:
        event['resize_required'] = 'True'
    else:
        event['resize_required'] = 'False'
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {
        "dr_region": "us-east-1",
        "stack_name": "jira-stack",
        "security_group_id": "sg-00000000000000000",
        "backmac_instance": "i-00000000000000000",
        "efs_used_space": "4",
    }

    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
