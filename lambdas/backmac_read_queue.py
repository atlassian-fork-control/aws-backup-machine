#!/usr/bin/env python3

import os
import pprint
import time

import boto3

import backmac_utils


def pop_from_queue(queue):
    print('popping stack name to backup off queue')
    queue_url = f"https://sqs.{os.environ['AWS_REGION']}.amazonaws.com/{boto3.client('sts').get_caller_identity().get('Account')}/{queue}"
    print(queue_url)
    sqs = boto3.client('sqs', region_name=os.environ['AWS_REGION'])
    sqs_message = sqs.receive_message(
        QueueUrl=queue_url, MaxNumberOfMessages=1, WaitTimeSeconds=3
    )
    try:
        sqs_message[u'Messages'][0][u'Body']
    except KeyError:
        print('no services remaining on the queue to process, backup run is complete')
        raise KeyError
    else:
        receipt_handle = sqs_message[u'Messages'][0][u'ReceiptHandle']
        try:
            sqs_message_delete = sqs.delete_message(
                QueueUrl=queue_url, ReceiptHandle=receipt_handle
            )
        except:
            pass
    return sqs_message[u'Messages'][0][u'Body']


def get_tags(stack_name):
    cfn = boto3.client('cloudformation', region_name=os.environ['AWS_REGION'])
    tags = cfn.describe_stacks(StackName=stack_name)['Stacks'][0]['Tags']
    return tags


def get_queue_stats(queue):
    print('Getting queue stats')
    queue_url = f"https://sqs.{os.environ['AWS_REGION']}.amazonaws.com/{boto3.client('sts').get_caller_identity().get('Account')}/{queue}"
    print(queue_url)
    sqs = boto3.client('sqs', region_name=os.environ['AWS_REGION'])
    sqs_queue_stats = sqs.get_queue_attributes(
        QueueUrl=queue_url, AttributeNames=['All']
    )
    print(sqs_queue_stats)


def lambda_handler(event, context):
    backmac_utils.store_lambda_env(event, context)
    get_queue_stats(event['queue_name'])
    event['stack_name'] = pop_from_queue(event['queue_name'])

    try:
        event['stack_name'], event['retry_count'] = event['stack_name'].split('.')
    except ValueError:
        # stack_name didn't contain a retry count
        pass

    event['stack_tags'] = get_tags(event['stack_name'])
    event['app_clustered'] = (
        next(
            (tag for tag in event['stack_tags'] if tag['Key'] == "clustered"),
            {'Value': 'true'},
        )['Value']
        == 'true'
    )
    event['cloned_stack'] = any(
        'cloned_from' in tag.values() for tag in event['stack_tags']
    )
    event['start_tstamp'] = time.time()

    print(f"starting backup processing for {event['stack_name']}")
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {"dr_region": "us-east-1"}
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
