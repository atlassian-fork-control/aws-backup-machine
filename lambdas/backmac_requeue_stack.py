#!/usr/bin/env python3

import os
import pprint

import boto3


def push_backups_to_queue(event, stack_name):
    sqs = boto3.client('sqs', region_name=os.environ['AWS_REGION'])
    retry = 1
    if 'retry_count' in event:
        retry += int(event['retry_count'])
    body = f'{stack_name}.{retry}'
    try:
        queue_response = sqs.send_message(
            QueueUrl=f"https://sqs.{os.environ['AWS_REGION']}.amazonaws.com/{boto3.client('sts').get_caller_identity().get('Account')}/backmac-queue",
            MessageBody=body,
        )
    except Exception as e:
        return e
    return queue_response


def read_from_queue():
    print('reading sqs queue to confirm requeue')
    queue_url = f"https://sqs.{os.environ['AWS_REGION']}.amazonaws.com/{boto3.client('sts').get_caller_identity().get('Account')}/backmac-queue"
    print(queue_url)
    sqs = boto3.client('sqs', region_name=os.environ['AWS_REGION'])
    sqs_message = sqs.receive_message(
        QueueUrl=queue_url, MaxNumberOfMessages=1, WaitTimeSeconds=3
    )
    pprint.pprint(sqs_message)

    return sqs_message[u'Messages'][0][u'Body']


def lambda_handler(event, context):
    stack_name = event['stack_name']
    if 'retry_count' in event:
        if int(event['retry_count']) > 2:
            raise Exception(
                f'Exceeded maximum number of retries for Stack {stack_name}'
            )
    push_backups_to_queue(event, stack_name)
    read_from_queue()
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {
        "stack_name": "jira-stack",
        "dr_region": "us-east-1",
        "security_group_id": "sg-abcdefgh",
        "backmac_instance": "i-00000000000000000",
        "efs_used_space": "2097320",
    }
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
