#!/usr/bin/env python3

import datetime
import os
import pprint
import time

import backmac_utils
import boto3


def get_stack_id(stack_name):
    cfn = boto3.client('cloudformation', region_name=os.environ['AWS_REGION'])
    stack_id = cfn.describe_stacks(StackName=stack_name)['Stacks'][0]['StackId']
    return stack_id


def lambda_handler(event, context):
    backmac_utils.store_lambda_env(event, context)
    stack_name = event['stack_name']
    backup_retention = event['backup_retention']
    timestamp = time.strftime('%Y%m%d%H%M')
    ec2 = boto3.resource('ec2', region_name=os.environ['AWS_REGION'])
    if 'app_clustered' in event and event['app_clustered'] is False:
        stack_id = get_stack_id(event['stack_name'])
        ebs_filters = [
            {'Name': 'tag:Name', 'Values': ['*local-home']},
            {'Name': 'tag:stack_id', 'Values': [stack_id]},
        ]
        ebs_vol_id = ec2.meta.client.describe_volumes(Filters=ebs_filters)['Volumes'][
            0
        ]['VolumeId']
        ebs_vol = ec2.Volume(ebs_vol_id)
        snapshot_desc = f'snapshot of {stack_name} EBS volume taken at {timestamp}'
    elif 'cloned_stack' in event and event['cloned_stack']:
        stack_id = get_stack_id(event['stack_name'])
        ebs_filters = [
            {'Name': 'tag:Name', 'Values': ['*DR shared-home volume']},
            {'Name': 'tag:aws:cloudformation:stack-id', 'Values': [stack_id]},
        ]
        ebs_vol_id = ec2.meta.client.describe_volumes(Filters=ebs_filters)['Volumes'][
            0
        ]['VolumeId']
        ebs_vol = ec2.Volume(ebs_vol_id)
        snapshot_desc = (
            f'snapshot of {stack_name} EBS shared-home DR volume taken at {timestamp}'
        )
    else:
        ebs_vol = ec2.Volume(event['ebs_backup_vol'])
        snapshot_desc = (
            f'snapshot of {stack_name} EBS backup volume taken at {timestamp}'
        )
    ebs_snap = ebs_vol.create_snapshot(Description=snapshot_desc)
    snapshot_obj = ec2.Snapshot(ebs_snap.id)
    snapshot_obj.create_tags(
        Tags=[
            {'Key': 'Name', 'Value': stack_name + '_ebs_snap_' + timestamp},
            {
                'Key': 'backup_lambda_log_group_name',
                'Value': '{}'.format(os.environ['AWS_LAMBDA_LOG_GROUP_NAME']),
            },
            {
                'Key': 'backup_lambda_log_stream_name',
                'Value': '{}'.format(os.environ['AWS_LAMBDA_LOG_STREAM_NAME']),
            },
            {'Key': 'backup_date', 'Value': '{}'.format(datetime.datetime.now())},
            {
                'Key': 'backup_delete_after',
                'Value': '{}'.format(
                    datetime.datetime.now() + datetime.timedelta(backup_retention)
                ),
            },
        ]
    )
    if 'stack_tags' in event and event['stack_tags']:
        snapshot_obj.create_tags(Tags=event['stack_tags'])

    # add rds snapshot_id to event
    event['ebs_snap'] = ebs_snap.id
    pprint.pprint(event)
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {
        "stack_name": "jira-stack",
        "dr_region": "us-east-1",
        "ebs_backup_vol": "vol-00000000000000000",
    }
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
