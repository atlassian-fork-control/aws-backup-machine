#!/usr/bin/env python3

'''
    Searches snapshots containing the 'backup_date', 'backup_delete_after' tag-keys and
    removes all snapshots that have expired as per the 'backup_delete_after' tag
'''

import os
import logging
from datetime import datetime, timedelta, timezone
from dateutil import parser

import boto3

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def clean_ec2_snapshots():
    # connect to AWS
    ec2_cli = boto3.client('ec2', region_name=os.environ['AWS_REGION'])
    logger.info(f'Connected to ec2 region: {os.environ["AWS_REGION"]}')

    # get a list of all snapshots owned by us with the backup_date, backup_delete_after tags
    snapshots = ec2_cli.describe_snapshots(
        Filters=[
            {'Name': 'tag-key', 'Values': ['backup_date']},
            {'Name': 'tag-key', 'Values': ['backup_delete_after']},
        ],
        OwnerIds=['self'],
    )

    # loop over snapshots and blow away anything passed its used by date
    print(f'Cleaning EC2 snapshots in {os.environ["AWS_REGION"]}')
    for snapshot in snapshots['Snapshots']:
        for tag in snapshot['Tags']:
            if tag['Key'] == 'Name':
                name = tag['Value']
            if tag['Key'] == 'backup_delete_after':
                backup_delete_after = tag['Value']
                # convert backup_delete_after string to a datetime object
                backup_delete_after = parser.parse(backup_delete_after)
            if tag['Key'] == 'backup_date':
                backup_date = tag['Value']

        if datetime.now() > backup_delete_after:
            print(
                f'Removing SnapshotId: {snapshot["SnapshotId"]}, name: {name}, backup_date: {backup_date}, backup_delete_after: {backup_delete_after}'
            )
            delete_snapshot_response = ec2_cli.delete_snapshot(
                SnapshotId=snapshot['SnapshotId']
            )


def clean_rds_snapshots(marker):
    # connect to AWS
    rds_cli = boto3.client('rds', region_name=os.environ['AWS_REGION'])
    logger.info(f'Connected to rds region: {os.environ["AWS_REGION"]}')
    # get a list of all snapshots owned by us with the backup_date, backup_delete_after tags
    snapshots = rds_cli.describe_db_snapshots(
        Marker=marker,
        SnapshotType='manual',
        # tag-key filters not supported on describe_db_snapshots :(
        # Filters=[
        #    {'Name': 'tag-key', 'Values': ['backup_date']},
        #    {'Name': 'tag-key', 'Values': ['backup_delete_after']},
        # ],
    )

    # boto RDS client isnt as mature as the EC2 client, tags are not available from boto3.client.describe_db_snapshots()
    # loop over snapshots, scrape arn, scrape tags from arn to determine if we can remove the snapshot
    # this is significantly more expensive than the EC2 cleanup
    print(f'Cleaning RDS snapshots in {os.environ["AWS_REGION"]}')
    for snapshot in snapshots['DBSnapshots']:
        tags = rds_cli.list_tags_for_resource(ResourceName=snapshot['DBSnapshotArn'])
        backup_date = 'NOT FOUND'
        backup_delete_after = 'NOT FOUND'
        if len(tags['TagList']) > 0:
            # snapshot has tags
            for tag in tags['TagList']:
                if 'backup_date' in tag.values():
                    backup_date = tag['Value']
                if 'backup_delete_after' in tag.values():
                    backup_delete_after = tag['Value']
            if 'NOT FOUND' not in backup_delete_after:
                backup_delete_after = parser.parse(backup_delete_after)
                if datetime.now() > backup_delete_after:
                    print(
                        f'Removing SnapshotId: {snapshot["DBSnapshotIdentifier"]}, backup_date: {backup_date}, backup_delete_after: {backup_delete_after}'
                    )
                    delete_rds_snapshot(snapshot['DBSnapshotIdentifier'])
        else:
            # there are no tags, flag it if it's older than 30 days.
            backup_delete_after = snapshot['SnapshotCreateTime'] + timedelta(days=30)
            snapshot_age = datetime.now(timezone.utc) - snapshot['SnapshotCreateTime']
            if datetime.now(timezone.utc) > backup_delete_after:
                print(
                    f'Found an old untagged Snapshot: SnapshotId: {snapshot["DBSnapshotIdentifier"]}, (no tags), backup_delete_after: {backup_delete_after} : {snapshot_age.days} days old'
                )
                # if you want to delete your old snapshots, uncomment the following line.
                # delete_rds_snapshot(snapshot['DBSnapshotIdentifier'])

    if snapshots.get('Marker') is None:
        return None
    else:
        return snapshots['Marker']


def delete_rds_snapshot(dbsnapshotid):
    delete_snapshot_response = boto3.client(
        'rds', region_name=os.environ['AWS_REGION']
    ).delete_db_snapshot(DBSnapshotIdentifier=dbsnapshotid)
    return delete_snapshot_response


def lambda_handler(event, context):
    '''
    main entry point
    '''
    logger.info('Starting handler')
    print('Loading lambda...\n---------------------')
    clean_ec2_snapshots()

    marker = ''
    while marker != None:
        marker = clean_rds_snapshots(marker)


if __name__ == '__main__':
    '''
    simulate the context and event and pass them in to the lambda_handler()
    '''
    context = 'context'
    event = {}

    '''
    simulate lambda env vars
    '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = '/aws/lambda/awsbackup_lambda'
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = '2999/00/11/[$LATEST]000 dummy stream name 000'
    os.environ['AWS_REGION'] = 'us-east-2'

    lambda_handler(event, context)
