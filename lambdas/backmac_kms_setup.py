#!/usr/bin/env python3

import copy
import os

import boto3

import backmac_utils


def get_backmac_role():
    cfn = boto3.client('cloudformation', region_name=os.environ['AWS_REGION'])
    exports_dict = cfn.list_exports()
    backmac_dict = [
        resource
        for resource in exports_dict['Exports']
        if resource['Name'] == 'BackmacRole'
    ]
    return backmac_dict[0]['Value']


def create_kms_key(region, tags):
    kms = boto3.client('kms', region_name=region)
    response = kms.create_key(
        Description=f'Backmac Created Key',
        KeyUsage='ENCRYPT_DECRYPT',
        Origin='AWS_KMS',
        BypassPolicyLockoutSafetyCheck=False,
        Tags=tags,
    )
    key_arn = response['KeyMetadata']['Arn']
    return key_arn


def create_kms_alias(region, key, alias):
    kms = boto3.client('kms', region_name=region)
    response = kms.create_alias(AliasName=f'alias/{alias}', TargetKeyId=key)
    return response


def create_kms_grant(region, key, backmac_role):
    kms = boto3.client('kms', region_name=region)
    response = kms.create_grant(
        KeyId=key,
        GranteePrincipal=backmac_role,
        RetiringPrincipal=backmac_role,
        Operations=['Decrypt', 'Encrypt'],
        Name=f'Backmac_Grant_for_{backmac_role}',
    )
    return response


def get_rds_encryption_key(stack_name):
    rds_name = backmac_utils.get_stack_resource_id(stack_name, 'DB')
    rds = boto3.client('rds', region_name=os.environ['AWS_REGION'])
    instance = rds.describe_db_instances(DBInstanceIdentifier=rds_name)
    try:
        key_arn = instance['DBInstances'][0]['KmsKeyId']
    except KeyError:
        print('RDS is not encrypted')
        key_arn = ''
    return key_arn


def get_stack_encryption_key(stack_name):
    cfn = boto3.client('cloudformation', region_name=os.environ['AWS_REGION'])
    stack_params = cfn.describe_stacks(StackName=stack_name)['Stacks'][0]['Parameters']
    try:
        key_arn = next(
            param for param in stack_params if param['ParameterKey'] == 'KmsKeyArn'
        )['ParameterValue']
    except StopIteration:
        print('Stack is not encrypted')
        key_arn = ''
    return key_arn


def get_dr_kms_aliases(dr_region, stack_name, marker):
    # we're looking for an alias called 'alias/{stack_name}-backmac-dr
    kms = boto3.client('kms', region_name=dr_region)
    if marker == '':
        aliases = kms.list_aliases()
    else:
        aliases = kms.list_aliases(Marker=marker)
    dr_key_id = ''
    for alias in aliases['Aliases']:
        if alias['AliasName'] == f'alias/{stack_name}-backmac-dr':
            dr_key_id = kms.describe_key(KeyId=alias['TargetKeyId'])['KeyMetadata'][
                'Arn'
            ]
            return None, dr_key_id

    return aliases.get('NextMarker'), dr_key_id


def get_rds_key_policy(key_id):
    kms = boto3.client('kms', region_name=os.environ['AWS_REGION'])
    response = kms.get_key_policy(KeyId=key_id, PolicyName='default')
    return response['Policy']


def rename_stack_tags(tags):
    # boto3 kms create_key uses different key and value names in the tags dict
    for tag in tags:
        tag['TagKey'] = tag.pop('Key')
        tag['TagValue'] = tag.pop('Value')
    return tags


def lambda_handler(event, context):
    backmac_utils.store_lambda_env(event, context)
    dr_key_id = ''
    event['source_kms_key_arn'] = get_stack_encryption_key(
        event['stack_name']
    ) or get_rds_encryption_key(event['stack_name'])
    backmac_role = get_backmac_role()
    kmstags = rename_stack_tags(copy.deepcopy(event['stack_tags']))

    if len(event['source_kms_key_arn']) > 1:
        event['stack_encrypted'] = 'true'
        try:
            create_kms_grant(
                os.environ['AWS_REGION'], event['source_kms_key_arn'], backmac_role
            )
        except Exception:
            raise Exception(
                f"Unable to create grant on kms key ({event['source_kms_key_arn']}) in source region ({os.environ['AWS_REGION']}) "
            )
        marker = ''
        while (marker is not None) and (dr_key_id == ''):
            marker, dr_key_id = get_dr_kms_aliases(
                event['dr_region'], event['stack_name'], marker
            )
        if dr_key_id == '':
            # couldn't find a key in dr region, create one
            dr_key_arn = create_kms_key(event['dr_region'], kmstags)
            create_kms_alias(
                event['dr_region'], dr_key_arn, f'{event["stack_name"]}-backmac-dr'
            )
            # since creating the alias doesn't return an arn, let's search for it
            marker = ''
            while (marker is not None) and (dr_key_id == ''):
                marker, dr_key_id = get_dr_kms_aliases(
                    event['dr_region'], event['stack_name'], marker
                )
            if dr_key_id == '':
                # we weren't able to find the new key, something has gone wrong
                raise Exception(
                    f"KMS Key missing from dr_region ({event['dr_region']}). Please create it with an alias of {event['stack_name']}-backmac-dr"
                )
            else:
                event['dr_kms_key_arn'] = dr_key_id
                try:
                    create_kms_grant(
                        event['dr_region'], event['dr_kms_key_arn'], backmac_role
                    )
                except Exception:
                    raise Exception(
                        f"Unable to create grant on kms key ({event['dr_kms_key_arn']}) in DR region ({event['dr_region']}) "
                    )
        else:
            event['dr_kms_key_arn'] = dr_key_id
            try:
                create_kms_grant(
                    event['dr_region'], event['dr_kms_key_arn'], backmac_role
                )
            except Exception:
                raise Exception(
                    f"Unable to create grant on kms key ({event['dr_kms_key_arn']}) in DR region ({event['dr_region']}) "
                )
    else:
        event['stack_encrypted'] = 'false'
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-1"

    event = {"dr_region": "us-east-2", "stack_name": "encryption-test"}
    context = ''
    result = lambda_handler(event, context)
