#!/usr/bin/env python3

import os
import pprint

import backmac_utils


def lambda_handler(event, context):
    backmac_utils.store_lambda_env(event, context)
    event['rds2dr_status'] = 'waiting'
    event['rds2dr_status'] = backmac_utils.check_rds_snapshot_state(
        event['rds_dr_snap'], event['dr_region']
    )
    print('returning successfully')
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = '2017/09/10/[$LATEST]078e6de35eda4343a4d44002f646831e'
    os.environ['AWS_REGION'] = "us-east-2"

    event = {
        "ebs_backup_vol": "vol-00000000000000000",
        "dr_region": "us-east-1",
        "stack_name": "jira-stack",
        "rds_snap_arn": "arn:aws:rds:us-west-2:000000000000:snapshot:jira-stack-snap-000000000000",
        "rds_snap": "jira-stack-snap-000000000000",
        "ebs_snap": "snap-00000000000000000",
        "rds_dr_snap": "dr-jira-stack-snap-000000000000",
    }
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
