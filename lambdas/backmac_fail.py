#!/usr/bin/env python3

import json
import os
import pprint
import time

import backmac_notify as notify
import backmac_utils


def lambda_handler(event, context):
    # we may or may not be coming out of a parallel execution block; merge the events if we are/can
    try:
        event = {**event[0], **event[1]}
    except IndexError:
        pass
    except KeyError:
        pass

    # look for the error at the root of the event or on a $.error ResultPath
    try:
        error = event['Error']
    except KeyError:
        try:
            error = event['error']['Error']
        except KeyError:
            error = "UnknownError"

    # try to parse the error as json; swallow any failures
    try:
        error = json.loads(error)
    except json.decoder.JSONDecodeError:
        pass

    # look for the cause at the root of the event or on a $.error ResultPath
    try:
        cause = event['Cause']
    except KeyError:
        try:
            cause = event['error']['Cause']
        except KeyError:
            cause = {
                'errorMessage': 'Unknown error; check SFN logs',
                'errorType': 'UnknownError',
            }

    # try to parse the cause as json; swallow any failures
    try:
        cause = json.loads(cause)
    except json.decoder.JSONDecodeError:
        pass
    except TypeError:
        pass

    event['error_details'] = {'error': error, 'cause': cause}

    try:
        event['error_details']['function_name'] = event['current_function_name']
        event['error_details']['log_group_name'] = event['current_log_group_name']
        event['error_details']['log_stream_name'] = event['current_log_stream_name']
    except KeyError:
        pass

    message = ['Iteration failed']
    if 'stack_name' in event:
        message.append(f'for {event["stack_name"]}')

    event['end_tstamp'] = time.time()
    print(notify.send("failure", ' '.join(message), event))
    return event


if __name__ == "__main__":
    import random

    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {
        "backmac_instance": "i-00000000000000000",
        "backup_machine": "some_backup_machine",
        "dr_region": "us-east-1",
        "ebs2dr_status": "completed",
        "ebs_backup_vol": "vol-00000000000000000",
        "ebs_dr_snap": "snap-00000000000000000",
        "ebs_snap": "snap-00000000000000000",
        "rds2dr_status": "waiting",
        "rds_snap": "jira-stack-snap-000000000000",
        "rds_snap_arn": "arn:aws:rds:us-west-2:000000000000:snapshot:jira-stack-snap-000000000000",
        "rsync_command_id": "836f6970-0fe1-4e10-a233-000000000000",
        "rsync_status": "Success",
        "run_id": "20190326-205156-900129",
        "security_group_id": "sg-abcdefgh",
        "stack_name": "jira-stack",
        "start_tstamp": time.time() - random.randint(30, 10000),
        "error": {
            'Error': 'IndexError',
            'Cause': '{"errorMessage": "list index out of range", "errorType": "IndexError", "stackTrace": [["/var/task/backmac_ebs_status.py", 30, "lambda_handler", "ebs_vol = get_backup_volume_id(event[\'stack_name\'])"], ["/var/task/backmac_ebs_status.py", 25, "get_backup_volume_id", "print((\\"backup volumeId is: \\", volumes[\'Volumes\'][0][\'VolumeId\']))"]]}',
        },
    }
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
