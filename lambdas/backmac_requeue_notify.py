#!/usr/bin/env python3

import os
import pprint

import backmac_notify as notify


def lambda_handler(event, context):
    message = f"Requeueing {event['stack_name']} for retry"
    print(notify.send("warning", message, event))
    return event


if __name__ == "__main__":
    '''
        simulate lambda env vars
        '''
    os.environ['AWS_LAMBDA_LOG_GROUP_NAME'] = "/aws/lambda/awsbackup_lambda"
    os.environ[
        'AWS_LAMBDA_LOG_STREAM_NAME'
    ] = "2999/00/11/[$LATEST]000 dummy stream name 000"
    os.environ['AWS_REGION'] = "us-east-2"

    event = {
        "backmac_instance": "i-00000000000000000",
        "ebs_backup_vol": "vol-00000000000000000",
        "dr_region": "us-east-1",
        "stack_name": "jira-stack",
        "security_group_id": "sg-abcdefgh",
        "rds2dr_status": "waiting",
        "rds_snap_arn": "arn:aws:rds:us-west-2:000000000000:snapshot:jira-stack-snap-000000000000",
        "ebs_dr_snap": "snap-00000000000000000",
        "ebs2dr_status": "completed",
        "rds_snap": "jira-stack-snap-000000000000",
        "ebs_snap": "snap-00000000000000000",
        "rsync_status": "Success",
        "rsync_command_id": "836f6970-0fe1-4e10-a233-000000000000",
    }
    context = ''
    result = lambda_handler(event, context)
    pprint.pprint(result)
