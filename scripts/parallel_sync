#!/usr/bin/env bash
# loop rsync until success bypassing known bad network error codes
# run this like : /usr/local/bin/parallel_sync /media/atl/confluence/shared-home /backup/confluence/shared-home
# will also work with remote destination, however you'll need to edit your sshd MaxStartups on the destination and limit the number of concurrent jobs to match (don't use j0)

# parms $1 = from location ( eg: /media/atl )
#       $2 = to location ( eg: /backup )
# note if params are not passed, assume from:/media/atl and to:/backup and workout the shared homes paths under those

# setup environment
export PATH=/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/home/ec2-user/.local/bin:/home/ec2-user/bin:/home/ec2-user/bin:$PATH

# we are going to use gnu parallel so lets install that if missing
export PATH=/usr/local/bin:$PATH
if ! which parallel; then
    wget -P /usr/local/bin/ https://git.savannah.gnu.org/cgit/parallel.git/plain/src/parallel
    chmod 755 /usr/local/bin/parallel
    cp /usr/local/bin/parallel /usr/local/bin/sem
fi

# if we still don't have parallel, try to download from a mirror
if ! which parallel; then
    mkdir /usr/local/bin/parallel-latest
    wget -P /usr/local/bin/ http://mirrors.kernel.org/gnu/parallel/parallel-latest.tar.bz2
    bzip2 -dc /usr/local/bin/parallel-latest.tar.bz2 | tar xvf - --directory /usr/local/bin/parallel-latest --strip-components=1
    cp /usr/local/bin/parallel-latest/src/parallel /usr/local/bin/parallel-latest/src/sem /usr/local/bin/
fi

# make sure we have a new enough version of rsync (3.1.0) that supports the --chown flag
if rsync --chown 2>&1 | grep "unknown option"; then
    # the version of rsync installed doesn't recognise the --chown flag
    if command -v yum; then
        rpm=$(curl -v --silent https://rpmfind.net/linux/fedora/linux/releases/29/Everything/x86_64/os/Packages/r/ 2>&1 | grep -o "rsync-3.1.*rpm")
        yum install -y "https://rpmfind.net/linux/fedora/linux/releases/29/Everything/x86_64/os/Packages/r/${rpm}"
    else
        echo "The version of rsync installed doesn't appear to support the --chown flag. Please upgrade to one that does (>=3.1.0) before retrying"
        exit 8
    fi
fi

set -e

# put parms into vars for readability
if [ -z "$1" ]; then
    # validate the default input and output paths are mounted and chose the correct paths
    if mount|grep /media/atl; then
        syncfrom=$(find /media/atl -maxdepth 2 -type d -regextype posix-egrep -regex '/media/atl/(confluence|jira|crowd)/(shared(-home)?)')
    else
        echo "$(basename $0) - ERROR, origin side of rsync is missing"
        exit 9
    fi
    if mount|grep /backup; then
        syncto=$(sed "s_/media/atl_/backup_g" <<< ${syncfrom})
        mkdir -p ${syncto}
    else
        echo "$(basename $0) - ERROR, destination side of rsync is missing"
        exit 9
    fi
else
    syncfrom=$1
    syncto=$2
fi

# define some wrappers for rsync
function rsync_with_delete {
  local from_dir=$1
  local to_dir=$2
  local owner=$3
  if [[ -d ${from_dir} ]]; then
    mkdir -p "${to_dir}"
    echo "performing rsync with delete on ${to_dir}"
    rsync --delete-during --verbose --no-links --times --recursive --itemize-changes --perms --group --owner --chown="$owner" --partial --progress "${from_dir}" "${to_dir}"
  fi
}

function rsync_ignore_vanished {
    # adapted from https://git.samba.org/?p=rsync.git;a=blob_plain;f=support/rsync-no-vanished;hb=HEAD
    rsync "${@}"
    ret=$?
    if [[ $ret == 24 ]]; then
        ret=0
    fi
    return $ret
}

function create_directory_structure {
    if [[ -e $1 ]]; then
        while read path ; do mkdir -p -- "${syncto}/$path" ; done < $1
    fi
}

# export the functions above so they're available to callers below
export -f rsync_with_delete
export -f rsync_ignore_vanished

# find the uid:gid of syncfrom and use it later
OWNER=$(stat -c '%u:%g' ${syncfrom})

# change to the relative location to sync from
cd ${syncfrom}

# find old 'layer' files in /tmp and remove them
find /tmp -name layer* -delete

# remove existing index snapshots, caches, plugins, and other specific items prior to sync to reduce the frequency of the backup vol running out of space: ADM-161344
cat << EOF > /tmp/layer0
export/indexsnapshots
index-snapshots
caches
analytics-logs
plugins/installed-plugins
EOF

# find a list of directories and setup those 3 layers deep for parallel processing - do not follow symlinks
find -P . -maxdepth 3 -mindepth 1 \
    -not \( -path '*/tmp' -prune \) \
    -not \( -path '*/dcl-*' -prune \) \
    -not \( -path '*/imgEffects' -prune \) \
    -not \( -path '*/thumbnails' -prune \) \
    -not \( -path '*/viewfile' -prune \) \
    -not \( -path '*/backups' -prune \) \
    -not \( -path '*/osgi-cache' -prune \) \
    -not \( -path '*/restore' -prune \) \
    -not \( -path '*/export/indexsnapshots' -prune \) \
    -not \( -path '*/index-snapshots' -prune \) \
    -not \( -path '*/caches' -prune \) \
    -not \( -path '*/analytics-logs' -prune \) \
    -not \( -path '*/plugins/installed-plugins' -prune \) \
    -type d -printf '%P\n'| awk -F'/' '{if(length($3)==0) {print > "/tmp/layer2"} else {print > "/tmp/layer3"}}'

# don't do layer4 for Confluence
if [[ -f ${syncfrom}/confluence.cfg.xml ]]; then
    # layer 3 isn't recursive, so let's call it layer 4
    mv /tmp/layer3 /tmp/layer4
else
    # otherwise run the layer4 search but do not follow symlinks
    find -P . -maxdepth 4 -mindepth 4 \
        -not \( -path '*/tmp' -prune \) \
        -not \( -path '*/dcl-*' -prune \) \
        -not \( -path '*/imgEffects' -prune \) \
        -not \( -path '*/thumbnails' -prune \) \
        -not \( -path '*/viewfile' -prune \) \
        -not \( -path '*/backups' -prune \) \
        -not \( -path '*/osgi-cache' -prune \) \
        -not \( -path '*/restore' -prune \) \
        -not \( -path '*export/indexsnapshots' -prune \) \
        -not \( -path '*index-snapshots' -prune \) \
        -not \( -path '*caches' -prune \) \
        -not \( -path '*analytics-logs' -prune \) \
        -not \( -path '*plugins/installed-plugins' -prune \) \
        -type d -printf '%P\n'| awk '{print > "/tmp/layer4"}'
fi

# create the first 3 layers of directories to prevent rsync issues
for layer in /tmp/layer0 /tmp/layer2 /tmp/layer3
  do create_directory_structure $layer
done

# run top layer rsync - files in $syncfrom only, no recursion into directories
rsync --verbose --itemize-changes --recursive --no-links --times --perms --group --owner --chown="$OWNER" --exclude='/*/' "${syncfrom}/" "$syncto"

# run the layer0 rsync with deletions in parallel at maximum number of cpu threads with recursion
if [[ -e /tmp/layer0 ]]; then
    /usr/local/bin/parallel -j0 --shuf --verbose -a /tmp/layer0 rsync_with_delete "${syncfrom}/{}/" "${syncto}/{}" "$OWNER"
fi

# run the layer2 rsync in parallel at maximum number of cpu threads no recursion
if [[ -e /tmp/layer2 ]]; then
    /usr/local/bin/parallel -j0 --shuf --verbose -a /tmp/layer2 rsync_ignore_vanished --verbose --itemize-changes --recursive --no-links --times --perms --group --owner --chown=${OWNER} --exclude=*/** $syncfrom/{}/ $syncto/{}
fi

# run the layer3 rsync in parallel at maximum number of cpu threads no recursion
if [[ -e /tmp/layer3 ]]; then
    /usr/local/bin/parallel -j0 --shuf --verbose -a /tmp/layer3 rsync_ignore_vanished --verbose --itemize-changes --recursive --no-links --times --perms --group --owner --chown=${OWNER} --exclude=*/** $syncfrom/{}/ $syncto/{}
fi

# run the layer4 rsync in parallel at maximum number of cpu threads with recursion
if [[ -e /tmp/layer4 ]]; then
    /usr/local/bin/parallel -j0 --shuf --verbose -a /tmp/layer4 rsync_ignore_vanished --verbose --itemize-changes --recursive --no-links --times --perms --group --owner --chown=${OWNER} $syncfrom/{}/ $syncto/{}
fi
